﻿using Lands.ViewModels;

namespace Lands.Infrastructure
{
    public class InstanceLocator
    {
        #region Attributes

        #endregion

        #region Properties
        public MainViewModel Main { get; set; }
        #endregion

        #region Constructor
        public InstanceLocator()
        {
            Main = new MainViewModel();
        }
        #endregion

        #region Methods

        #endregion

        #region Commands

        #endregion
    }
}
